FROM gcr.io/kaniko-project/executor:debug

RUN chgrp -R 0 /kaniko /var /cache &&\
    chmod -R g=u /kaniko
